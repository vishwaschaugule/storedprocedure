CREATE DATABASE Position
USE Position

CREATE TABLE EmpPosition(PositionId int PRIMARY KEY,Description nvarchar(50),
Role nvarchar(20),Band nvarchar(10),Designation nvarchar(15),Skill nvarchar(50));

INSERT INTO EmpPosition VALUES(101,'Project Management','Manager','Band 2','Project Manager','asp.net');
Select * From EmpPosition;
--1.Insert Stored Procedure
CREATE PROCEDURE InsertProc
(
@positionId integer,
@description nvarchar(50),
@role nvarchar(20),
@band nvarchar(10),
@designation nvarchar(15),
@skill nvarchar(50) 
)
AS
BEGIN
	Insert into EmpPosition(PositionId,Description,Role,Band,Designation,Skill)
	values(@positionId,@description,@role,@band,@designation,@skill)  
END

--2.Update Stored Procedure
CREATE PROCEDURE UpdateProc
(
@positionId integer,
@description nvarchar(50),
@role nvarchar(20),
@band nvarchar(10),
@designation nvarchar(15),
@skill nvarchar(50) 
)
AS
BEGIN
	Update EmpPosition
	set PositionId=@positionId,Description= @description,Role= @role,Band =@band,Designation= @designation,Skill= @skill  
END

--3.Delete Stored Procedure
CREATE PROCEDURE DeleteProc
(
@positionId integer,
@description nvarchar(50),
@role nvarchar(20),
@band nvarchar(10),
@designation nvarchar(15),
@skill nvarchar(50) 
)
AS
BEGIN
	Delete From EmpPosition
	where PositionId=@positionId  
END

--4.GetAll Stored Procedure
CREATE PROCEDURE GetAllProc
(
@positionId integer,
@description nvarchar(50),
@role nvarchar(20),
@band nvarchar(10),
@designation nvarchar(15),
@skill nvarchar(50) 
)
AS
BEGIN
	Select * From EmpPosition
	  
END

--5.GetByPositionId Stored Procedure
Alter PROCEDURE GetByPositionIdProc
(
@positionId integer
)
AS
BEGIN
	Select * From EmpPosition
	where PositionId=@positionId  
END

--6.GetByDesignation Stored Procedure
Alter PROCEDURE GetByDesignationProc
(
@designation nvarchar(15) 
)
AS
BEGIN
	Select * From EmpPosition
	where Designation=@designation  
END
